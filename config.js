module.exports = {
    'name'  : 'PostPreview',
    'camel' : 'PostPreview',
    'slug'  : 'post-preview',
    'dob'   : 'Features_12_1440',
    'desc'  : 'A list of individual posts with the ability to showcase one.',
}