import Vue from 'vue'
import firPostPreview from './view'

class PostPreview extends window.HTMLDivElement {

  constructor (...args) {
      const self = super(...args)
      self.init()
      return self
  }

  init () {
      this.props = this.getInitialProps()
      this.resolveElements()
    }

  getInitialProps () {
      let data = {}
      try {
          data = JSON.parse(this.querySelector('script[type="application/json"]').text())
      } catch (e) {}
      return data
  }

  resolveElements () {
    this.dataID = this.dataset.id;
  }

  connectedCallback () {
      this.initPostPreview()
  }

  initPostPreview () {
      const { options } = this.props
      const config = {

      }

      this.app = new Vue({
        el: `[data-id="${this.dataID}"] .fir--vue`,
        components: {
          firPostPreview
        }
      })

      // console.log("Init: PostPreview")
  }

}

window.customElements.define('fir-post-preview', PostPreview, { extends: 'section' })
