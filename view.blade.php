<!-- Start PostPreview -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A list of individual posts with the ability to showcase one. -->
@endif
<section class="{{ $block->classes }}" is="fir-post-preview" id="{{ $pinecone_id ?? '' }}" data-id="PostPreview{{ Fir\Utils\Helpers::getUniqueInt(1000, 9000) }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="post-preview {{ $theme }} {{ $text_align }} {{ $flip }} post-preview__{{ $post_type }}">
      @if($title)
      <h2 class="post-preview__title">{{ $title }}</h2>
      @endif
      <div class="fir--vue">
        <fir-post-preview :posts="{{ json_encode($posts) }}" :fields="{{ json_encode($custom) }}"></fir-post-preview>
      </div>
  </div>
</section>
<!-- End PostPreview -->
