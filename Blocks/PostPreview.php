<?php

namespace Fir\Pinecones\PostPreview\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class PostPreview extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Post Preview';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'PostPreview.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['PostPreview'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'title'=> null,
            'post_type' => [
                'news' => 'News',
                'services' => 'Services',
                'projects' => 'Projects',
                'faqs' => 'FAQs'
            ]
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['post_type'] = ($data['content']['post_type']) ?: 'posts';
        $data['posts'] = get_posts(['post_type' => $data['post_type'], 'numberposts' => -1]);

        // $fields = get_post_meta($data['posts'][0]->ID);
        $data['custom'] = \Fir\Utils\Helpers::getMeta($data['posts']);

        $data['title'] = ($data['content']['title']) ?: $this->defaults['content']['title'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'post-preview--flip' : '';
        $data['text_align'] = 'post-preview--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $post = new FieldsBuilder('PostPreview');

        $post
            ->addGroup('content', [
                'label' => 'Post Preview',
                'layout' => 'block'
            ])
                ->addText('title')
                ->addSelect('post_type', [
                    'choices' => $this->defaults['content']['post_type']
                ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $post->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/postPreview.css', asset('styles/fir/Pinecones/PostPreview/style.css')->uri(), false, null);
        // wp_enqueue_script('sage/postPreview.js', asset('scripts/fir/Pinecones/PostPreview/script.js')->uri(), [], null, true);
    }
}
